<?php
    class Animal {
        public $name;
        public $legs = 2;
        public $cold_blooded = "false";
        public function __construct($name)
        {
            $this->name = $name;
        }

        public function a()
        {
            # code...
        }
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())\

        public function get_name(){
            return "nama : ".$this->name."<br>";
        }
        public function get_legs(){
            return "jumlah kaki : ".$this->legs."<br>";
        } 
        public function get_cold_blooded(){
            return "berdarah dingin : ".$this->cold_blooded."<br>";
        }

    }
?>