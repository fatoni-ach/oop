<?php
    require('animal.php');
    require('Frog.php');
    require('Ape.php');
    $sheep = new Animal("shaun");

    echo $sheep->get_name(); // "shaun"
    echo $sheep->get_legs(); // 2
    echo $sheep->get_cold_blooded(); // false

    // index.php
    echo "<br>";
    $sungokong = new Ape("kera sakti");
    echo $sungokong->get_name(); 
    echo $sungokong->get_legs(); 
    echo $sungokong->get_cold_blooded(); 
    $sungokong->yell(); // "Auooo"

    echo "<br>";
    $kodok = new Frog("buduk");
    echo $kodok->get_name(); 
    echo $kodok->get_legs(); 
    echo $kodok->get_cold_blooded(); 
    $kodok->jump() ; // "hop hop"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())\
?>